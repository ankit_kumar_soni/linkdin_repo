import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { BaseApiService } from './api/base-api.service';

@Injectable({
  providedIn: 'root',
})
export class UserService {
  constructor(private baseApi: BaseApiService) {}

  postUserData(userInfo: any) {
    return this.baseApi.post(`${environment.baseUrl}/users.json`, userInfo);
  }

  getUserData() {
    return this.baseApi.get(`${environment.baseUrl}/users.json`);
  }

  savePostData(postInfo: any) {
    return this.baseApi.post(`${environment.baseUrl}/post.json`, postInfo);
  }
}
