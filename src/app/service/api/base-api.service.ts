import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class BaseApiService {
  private defalutHeader: HttpHeaders;

  constructor(private http: HttpClient) {
    this.defalutHeader = new HttpHeaders({});
  }

  get(url: string) {
    const response: Observable<any> = this.http.get(url, {
      headers: this.defalutHeader,
    });
    return response;
  }

  getNews() {
    return this.http.get(
      'https://intest-7e450-default-rtdb.firebaseio.com/news.json'
    );
  }
  getCourses() {
    return this.http.get(
      'https://intest-7e450-default-rtdb.firebaseio.com/courses.json'
    );
  }
  getPosts() {
    return this.http.get(
      'https://intest-7e450-default-rtdb.firebaseio.com/post.json'
    );
  }
  getUsers() {
    return this.http.get(
      'https://intest-7e450-default-rtdb.firebaseio.com/users.json'
    );
  }
  putComment(id: string, payload: object) {
    return this.http.put(
      `https://intest-7e450-default-rtdb.firebaseio.com/post/${id}.json`,
      payload
    );
  }

  post(url: string, payload: any): Observable<any> {
    const response: Observable<any> = this.http.post(url, payload, {
      headers: this.defalutHeader,
    });
    return response;
  }
}
