import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SignupComponent } from './pages/user/signup/signup.component';
import { LoginComponent } from './pages/user/login/login.component';

import { HomeComponent } from './pages/user/home/home.component';
import { ToolBarComponent } from './pages/story/tool-bar/tool-bar.component';
import { HttpClientModule } from '@angular/common/http';
import { FeedsComponent } from './pages/story/feeds/feeds.component';
import { ProfileCardComponent } from './pages/story/profile-card/profile-card.component';
import { NewsComponent } from './pages/story/news/news.component';
import { LearningComponent } from './pages/story/learning/learning.component';
import { PostComponent } from './pages/story/post/post.component';
import { MynetworkComponent } from './pages/story/mynetwork/mynetwork.component';
import { ScrollspyComponent } from './pages/story/scrollspy/scrollspy.component';
import { EventComponent } from './pages/story/event/event.component';
import { PostModalComponent } from './pages/story/post-modal/post-modal.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { WorkModalComponent } from './pages/story/work-modal/work-modal.component';
import { PostcardsComponent } from './pages/story/postcards/postcards.component';
import { ProfileComponent } from './pages/story/profile/profile.component';
import { NgPipesModule } from 'ngx-pipes';
import { environment } from 'src/environments/environment';
import { AngularFireStorageModule } from '@angular/fire/storage';
import { AngularFireModule } from '@angular/fire';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    SignupComponent,
    HomeComponent,
    ToolBarComponent,
    FeedsComponent,
    ProfileCardComponent,
    NewsComponent,
    LearningComponent,
    PostComponent,
    MynetworkComponent,
    ScrollspyComponent,
    EventComponent,
    PostModalComponent,
    WorkModalComponent,
    PostcardsComponent,
    ProfileComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    FormsModule,
    HttpClientModule,
    NgbModule,
    NgPipesModule,
    AngularFireStorageModule,
    AngularFireModule.initializeApp(environment.firebaseConfig, 'cloud'),
  ],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
