import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import{ FormGroup, FormControl, FormBuilder, FormArray,Validators} from '@angular/forms';
import { LocalStorageService } from 'src/app/service/localStorage/local-storage.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  constructor(private router:Router,private fb:FormBuilder, private localStorage : LocalStorageService) { }
  profileForm:any;

  ngOnInit(): void {
    if(this.localStorage.getItem('loggedInUser')) {
      this.router.navigate(['feed']);
    }
    this.profileForm=new FormGroup({
      'search':new FormControl(),
      'location': new FormControl(),

    })
  }
  signinHandler(){
    this.router.navigate(['/login'])
    console.log("signin")
  }
  signUpHandler(){
    this.router.navigate(['/signup'])
  }
  save(){
    console.warn(this.profileForm.value)
    alert("search successfully");
   
  }
}
