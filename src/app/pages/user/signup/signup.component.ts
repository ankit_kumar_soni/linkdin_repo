import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { UserService } from 'src/app/service/user.service';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.scss'],
})
export class SignupComponent implements OnInit {
  signupForm: any;
  userDetailsForm: any;
  userFlag = false;
  successFlag = false;
  userId: string[] = [];
  register = true;

  constructor(private userService: UserService, private router: Router) {}

  ngOnInit(): void {
    if (localStorage.getItem('loggedInUser')) {
      this.router.navigate(['feed']);
    }
    this.signupForm = new FormGroup({
      email: new FormControl('', [
        Validators.required,
        Validators.pattern(
          /^(\d{10}|\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3}))$/
        ),
      ]),
      password: new FormControl('', [
        Validators.required,
        Validators.minLength(6),
      ]),
    });

    this.userDetailsForm = new FormGroup({
      firstName: new FormControl('', Validators.required),
      lastName: new FormControl('', Validators.required),
    });
  }

  signUp() {
    if (this.signupForm.invalid) {
      if (this.email.errors.pattern) {
        alert('Please Enter Valid Email/Phone number');
      }
      this.signupForm.markAllAsTouched();
    }

    if (this.signupForm.valid) {
      this.userService.getUserData().subscribe((data) => {
        this.userId = Object.keys(data);
        const findArr: any = this.userId.filter(
          (id) => data[id].email === this.signupForm.get('email')?.value
        );
        console.log(findArr);

        if (findArr.length === 1) {
          alert('Email Already Registered');
          this.register = false;
        } else {
          this.register = true;
          this.userFlag = true;
        }
      });

      console.log(this.signupForm.value);
    }
  }

  userDetails() {
    if (this.userDetailsForm.invalid) this.userDetailsForm.markAllAsTouched();

    if (this.userDetailsForm.valid) {
      if (this.register) {
        var payload = {
          ...this.signupForm.value,
          ...this.userDetailsForm.value,
        };
        this.userService.postUserData(payload).subscribe((response) => {
          console.log(response.name);
          if (response.name != null) {
            this.successFlag = true;
            alert('Registered Successfully');
          }

          console.log(JSON.stringify(response));
        });
      }
    }
  }

  get email() {
    return this.signupForm.get('email');
  }

  get password() {
    return this.signupForm.get('password');
  }

  get firstName() {
    return this.userDetailsForm.get('firstName');
  }

  get lastName() {
    return this.userDetailsForm.get('lastName');
  }
}
