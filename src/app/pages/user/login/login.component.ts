import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { UserService } from 'src/app/service/user.service';
import { LocalStorageService } from 'src/app/service/localStorage/local-storage.service';

import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements OnInit {
  currentVisibility = false;
  constructor(
    private UserService: UserService,
    private router: Router,
    private localStorage: LocalStorageService
  ) {}

  userId: string[] = [];
  loggedIn = false;
  incorrectInputs = false;
  emailIsInvalid = false;
  currentUserId = '';
  loggedInUser: any = null;

  ngOnInit(): void {
    if (localStorage.getItem('loggedInUser')) {
      this.router.navigate(['feed']);
    }
  }
  profileForm = new FormGroup({
    emailOrPhone: new FormControl('', [
      Validators.required,
      Validators.minLength(4),
      Validators.pattern(
        /^(\d{10}|\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3}))$/
      ),
    ]),
    password: new FormControl('', Validators.required),
  });

  get emailOrPhone() {
    return this.profileForm.get('emailOrPhone');
  }
  get password() {
    return this.profileForm.get('password');
  }

  signIn(e: any) {
    this.incorrectInputs = false;

    if (String(this.emailOrPhone)?.length > 3) this.emailIsInvalid = false;

    if (e.code === 'Enter' || e.type === 'click') {
      if (this.profileForm.invalid) {
        if (this.emailOrPhone?.errors?.pattern) {
          this.emailIsInvalid = true;
        } else {
          this.emailIsInvalid = false;
        }
        this.profileForm.markAllAsTouched();
      } else {
        this.loginCheck();
      }
    }
  }

  navToSignup() {
    this.router.navigate(['signup']);
  }

  loginCheck() {
    this.UserService.getUserData().subscribe((data) => {
      this.userId = Object.keys(data);

      this.userId.forEach((user) => {
        if (
          (data as any)[user].email ===
            this.profileForm.get('emailOrPhone')?.value &&
          (data as any)[user].password ===
            this.profileForm.get('password')?.value
        ) {
          this.loggedInUser = (data as any)[user];
          this.loggedInUser.id = user;
          this.loggedIn = true;
        }
      });
      if (this.loggedIn === true) {
        this.localStorage.setItem('loggedInUser', this.loggedInUser);
        this.router.navigate(['feed']);

        this.loggedIn = false;
      } else {
        this.incorrectInputs = true;
      }
    });
  }
}
