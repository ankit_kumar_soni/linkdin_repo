import { Component, OnInit } from '@angular/core';
import { LocalStorageService } from 'src/app/service/localStorage/local-storage.service';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit {

  constructor(private LocalStorageService : LocalStorageService) { }
  userData : any;
  ngOnInit(): void {
    if(this.LocalStorageService.getItem('loggedInUser'))
    {this.userData =JSON.parse(this.LocalStorageService.getItem('loggedInUser') || '{}');
    }
  }

}
