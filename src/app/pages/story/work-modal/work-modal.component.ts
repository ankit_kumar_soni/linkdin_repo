import { Component, OnInit } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-work-modal',
  templateUrl: './work-modal.component.html',
  styleUrls: ['./work-modal.component.scss'],
})
export class WorkModalComponent implements OnInit {
  closeResult: any;

  constructor(private modalService: NgbModal) {}

  ngOnInit(): void {}

  openScrollableContent(longContent: any) {
    this.modalService.open(longContent, {
      scrollable: true,
    });
  }
}
