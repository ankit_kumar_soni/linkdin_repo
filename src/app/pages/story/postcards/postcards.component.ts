import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { avatars } from 'src/assets/images';
import * as moment from 'moment';
import { LocalStorageService } from 'src/app/service/localStorage/local-storage.service';
import { BaseApiService } from 'src/app/service/api/base-api.service';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'app-postcards',
  templateUrl: './postcards.component.html',
  styleUrls: ['./postcards.component.scss'],
})
export class PostcardsComponent implements OnInit {
  @Output('changeEv') onchange: EventEmitter<any> = new EventEmitter();
  @Input() allPosts: any;
  @Input() postKey: any;
  @Input() allUsers: any;
  @Input() index: number = 0;
  safeUrl: any;
  like = false;
  hasImage = true;
  toggleComment = true;
  avatars = avatars;
  numberOfComments: number = 0;
  editPost: object[] = [];
  commentValue = '';
  likes = [];
  likedOrNot = [];
  currentPost: any = null;
  editLikes: [] = [];
  currentUser = JSON.parse(this.localStorage.getItem('loggedInUser') + '');
  constructor(
    private localStorage: LocalStorageService,
    private baseapi: BaseApiService,
    private sanitizer: DomSanitizer
  ) {}

  ngOnInit(): void {
    this.currentPost = this.allPosts[this.postKey];
    this.likes = this.currentPost.likes;

    this.numberOfComments = this.currentPost.comments.length;

    const likeId = this.likes.filter(
      (likeData) => likeData === this.currentUser.id
    );
    if (likeId.length) this.like = true;
  }

  getNameById(id: string) {
    if (id)
      return this.allUsers[id].firstName + ' ' + this.allUsers[id].lastName;
    return '';
  }

  momentCalc(date: string) {
    return moment(date).fromNow();
  }

  addComment() {
    if (this.commentValue.length) {
      this.editPost = this.allPosts[this.postKey].comments;
      this.editPost.push({
        comment: this.commentValue,
        date: new Date().toISOString(),
        userId: JSON.parse(localStorage.getItem('loggedInUser') || '{}').id,
      });
      this.allPosts[this.postKey].comments = this.editPost;

      this.baseapi
        .putComment(this.postKey, this.allPosts[this.postKey])
        .subscribe(
          (data) => {
            this.commentValue = '';

            this.onchange.emit(data);
          },
          (error) => console.log(error.message)
        );
    }
  }

  changeLike() {
    let likeId = this.currentPost.likes.filter(
      (likeData: any) => likeData === this.currentUser.id
    );
    if (!likeId.length) {
      this.currentPost.likes.push(this.currentUser.id);
    } else {
      let index = this.currentPost.likes.findIndex(
        (data: any) => data === this.currentUser.id
      );
      this.currentPost.likes.splice(index, 1);
    }
    this.baseapi.putComment(this.postKey, this.currentPost).subscribe(
      (data) => {
        this.onchange.emit(this.currentPost);
        this.like = !this.like;
      },
      (e) => {}
    );
  }
  getSafeUrl(url: string) {
    return (this.safeUrl = this.sanitizer.bypassSecurityTrustResourceUrl(url));
  }
}
