import { Component, OnInit } from '@angular/core';
import { BaseApiService } from 'src/app/service/api/base-api.service';
// import { avatars } from 'src/assets/images';

@Component({
  selector: 'app-feeds',
  templateUrl: './feeds.component.html',
  styleUrls: ['./feeds.component.scss'],
})
export class FeedsComponent implements OnInit {
  constructor(private baseApiService: BaseApiService) {}
  public allPosts: any;
  public allUsers: any;
  public keys: string[] = [];
  loading = false;
  ngOnInit(): void {
    this.getPosts();
  }
  getPosts(): void {
    setTimeout(() => {
      this.loading = true;
    }, 0);
    this.baseApiService.getPosts().subscribe(
      (data) => {
        this.keys = Object.keys(data);
        this.allPosts = data;
        this.loading = false;
      },
      (error) => console.log(error)
    );
    this.baseApiService.getUsers().subscribe((data) => (this.allUsers = data));
  }
}
