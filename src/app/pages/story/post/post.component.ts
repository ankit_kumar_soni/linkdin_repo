import {
  Component,
  EventEmitter,
  OnInit,
  Output,
  ViewChild,
} from '@angular/core';
import { AngularFireStorage } from '@angular/fire/storage';
import { PostModalComponent } from '../post-modal/post-modal.component';

@Component({
  selector: 'app-post',
  templateUrl: './post.component.html',
  styleUrls: ['./post.component.scss'],
})
export class PostComponent implements OnInit {
  attachment = null;
  attachmentType: string = '';
  tempAttachmentType: string = 'normal';
  @Output('changeEv') onChange: EventEmitter<any> = new EventEmitter();
  constructor() {}

  ngOnInit(): void {}

  openPost(e: any) {
    this.attachmentType = this.tempAttachmentType;
    this.attachment = e;
  }

  getPosts(e?: any) {
    this.onChange.emit(e);
  }
}
