import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-event',
  templateUrl: './event.component.html',
  styleUrls: ['./event.component.scss']
})
export class EventComponent implements OnInit {

  constructor( private router:Router) { }
ind:any;
public n = 5;
more=true;
  ngOnInit(): void {
  }
  DiscoverMore(){
    this.router.navigate(['mynetwork']);
  }
  showMore() {
    if (this.n === 5) {
      this.n = 0;
      this.more = false;
    } else {
      this.n = 5;
      this.more = true;
    }
    this.ngOnInit();
  }
  
}

