import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import {LocalStorageService} from "src/app/service/localStorage/local-storage.service"
@Component({
  selector: 'app-profile-card',
  templateUrl: './profile-card.component.html',
  styleUrls: ['./profile-card.component.scss']
})
export class ProfileCardComponent implements OnInit {

  constructor(private LocalStorageService : LocalStorageService,private router:Router) { }
  userData : any;
  ngOnInit(): void {
    if(this.LocalStorageService.getItem('loggedInUser'))
    {this.userData =JSON.parse(this.LocalStorageService.getItem('loggedInUser') || '{}');
    }
  }
  profile(){
    this.router.navigate(['profile'])
  }

}
