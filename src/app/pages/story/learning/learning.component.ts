import { Component, OnInit } from '@angular/core';
import { BaseApiService} from 'src/app/service/api/base-api.service';
import * as moment from 'moment';

@Component({
  selector: 'app-learning',
  templateUrl: './learning.component.html',
  styleUrls: ['./learning.component.scss'],
})
export class LearningComponent implements OnInit {
  constructor(private baseApiService: BaseApiService) {}
  public title: any;
  public values: string[] = [];

  ngOnInit(): void { 
    this.baseApiService.getCourses().subscribe(
    (data) => {
      this.values = Object.keys(data);
      this.title = data;
   
    },
    (error) => console.log(error)
  );
}
}

