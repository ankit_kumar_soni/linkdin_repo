import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { LocalStorageService } from 'src/app/service/localStorage/local-storage.service';

@Component({
  selector: 'app-tool-bar',
  templateUrl: './tool-bar.component.html',
  styleUrls: ['./tool-bar.component.scss'],
})
export class ToolBarComponent implements OnInit {
  constructor(
    private LocalStorageService: LocalStorageService,
    public router: Router
  ) {}
  userData: any;
  ngOnInit(): void {
    if (this.LocalStorageService.getItem('loggedInUser')) {
      this.userData = JSON.parse(
        this.LocalStorageService.getItem('loggedInUser') || '{}'
      );
    }
  }
  logout() {
    this.router.navigate(['']);
    this.LocalStorageService.removeItem('loggedInUser');
  }
  signOut() {
    this.router.navigate(['']);
    this.LocalStorageService.removeItem('loggedInUser');
  }
  profile() {
    this.router.navigate(['profile']);
  }
}
