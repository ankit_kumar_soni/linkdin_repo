import { Component, OnInit } from '@angular/core';
import { BaseApiService } from 'src/app/service/api/base-api.service';
import * as moment from 'moment';
@Component({
  selector: 'app-news',
  templateUrl: './news.component.html',
  styleUrls: ['./news.component.scss'],
})
export class NewsComponent implements OnInit {
  constructor(private baseApiService: BaseApiService) {}

  public allNews: any;
  public keys: string[] = [];
  public slicedKeys: string[] = [];
  public n = 5;
  more = true;

  ngOnInit(): void {
    this.baseApiService.getNews().subscribe(
      (data) => {
        this.keys = Object.keys(data);
        this.allNews = data;

        this.slicedKeys = this.keys.slice(this.n);
      },
      (error) => console.log(error)
    );
  }
  momentCalc(date: string) {
    return moment(date).fromNow();
  }
  showMore() {
    if (this.n === 5) {
      this.n = 0;
      this.more = false;
    } else {
      this.n = 5;
      this.more = true;
    }
    this.ngOnInit();
  }
}
