import {
  Component,
  ElementRef,
  EventEmitter,
  Input,
  OnInit,
  Output,
  ViewChild,
} from '@angular/core';
import { AngularFireStorage } from '@angular/fire/storage';
import {
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from '@angular/forms';
import { ModalDismissReasons, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable } from 'rxjs';
import { map, finalize } from 'rxjs/operators';
import { LocalStorageService } from 'src/app/service/localStorage/local-storage.service';
import { UserService } from 'src/app/service/user.service';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'app-post-modal',
  templateUrl: './post-modal.component.html',
  styleUrls: ['./post-modal.component.scss'],
})
export class PostModalComponent implements OnInit {
  @ViewChild('postmodal') postModal: ElementRef | undefined;
  @Input() attachment = null;
  @Input() attachmentType: string = 'normal';
  @Output('changeEv') onChange: EventEmitter<any> = new EventEmitter();

  closeResult = '';
  currentUser = JSON.parse(this.localStorage.getItem('loggedInUser') + '');
  fileRef: any;
  fb: any;
  downloadURL: Observable<string> | undefined;
  postDisabled = false;
  safeUrl: any;

  constructor(
    private modalService: NgbModal,
    private storage: AngularFireStorage,
    private formBuilder: FormBuilder,
    private userService: UserService,
    private localStorage: LocalStorageService,
    private sanitizer: DomSanitizer
  ) {}

  ngOnInit(): void {
    // if(this.localStorage.getItem('loggedInUser'))
    // {this.userData =JSON.parse(this.LocalStorageService.getItem('loggedInUser') || '{}');
    // }
  }

  modalForm = this.formBuilder.group({
    content: [''],
    attachment: [''],
    privacy: ['anyone'],
    likes: this.formBuilder.array(['new']),
    userId: [''],
    date: [''],
    type: ['normal'],
    comments: this.formBuilder.array(['new']),
  });

  ngOnChanges() {
    this.modalForm?.patchValue({
      type: this.attachmentType,
    });
    if (this.attachment) {
      this.modalService.dismissAll();
      this.open();
      this.onFileSelected(this.attachment);
    }
  }

  typeOfPost(type: string) {
    this.modalForm.patchValue({
      type: type,
    });
  }
  open(postmodal?: any) {
    this.modalService
      .open(this.postModal, { centered: true, size: 'lg' })
      .result.then(
        (result) => {
          this.closeResult = `Closed with: ${result}`;
        },
        (reason) => {
          this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
          this.deleteImg();
          this.modalForm?.patchValue({
            content: '',
            attachment: '',
            privacy: 'anyone',
            likes: ['new'],
            userId: '',
            date: '',
            type: 'normal',
            comments: ['new'],
          });
        }
      );
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }

  SavePost() {
    this.modalForm.patchValue({
      attachment: this.fb || '',
      userId: this.currentUser.id,
      likes: ['new'],
      comments: ['new'],
      date: new Date().toISOString(),
    });

    this.userService.savePostData(this.modalForm.value).subscribe(
      (response) => {
        if (response.name != null) {
          alert('Posted Data Successfully');
          this.fb = null;
          this.onChange.emit(response);
          this.modalService.dismissAll();
        }
      },
      (e) => {
        alert('Something went wrong!! please try again');
      }
    );
  }

  //selecting file
  onFileSelected(event: any) {
    setTimeout(() => {
      this.postDisabled = true;
    }, 0);
    var n = Date.now();
    const file = event.target.files[0];
    const filePath = `postImages/${n}`;
    this.fileRef = this.storage.ref(filePath);
    const task = this.storage.upload(`postImages/${n}`, file);

    task
      .snapshotChanges()
      .pipe(
        finalize(() => {
          this.downloadURL = this.fileRef.getDownloadURL();
          this.downloadURL?.subscribe((url) => {
            if (url) {
              this.fb = url;
              // this.fb = url;
            }
            this.postDisabled = false;
          });
        })
      )
      .subscribe((url) => {
        if (url) {
        }
      });
  }

  //Deleting image
  deleteImg() {
    if (this.fb) this.fileRef.delete();
    this.attachmentType = 'normal';
    this.fb = null;
  }

  getSafeUrl(url: string) {
    return (this.safeUrl = this.sanitizer.bypassSecurityTrustResourceUrl(url));
  }

  get content() {
    return this.modalForm.get('content')?.value || '';
  }

  get privacy() {
    return this.modalForm.get('privacy')?.value || '';
  }

  // addHash() {
  //   alert('hi');
  // }
}
