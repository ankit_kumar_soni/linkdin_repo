// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  baseUrl: 'https://intest-7e450-default-rtdb.firebaseio.com/',
  firebaseConfig: {
    apiKey: 'AIzaSyCqreyBkvONijIx-e2d4gwv3u9Grk_cDoo',
    authDomain: 'intest2-50489.firebaseapp.com',
    projectId: 'intest2-50489',
    storageBucket: 'intest2-50489.appspot.com',
    messagingSenderId: '241094930563',
    appId: '1:241094930563:web:0961503fe0cbd86e0f6bb6',
    measurementId: 'G-QD4K15QW5V',
  },
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
